from cell import Cell
from config import *
import pygame
import random

class Maze():
    def __init__(self):
        self.map = [[0 for _ in range(MAZE_SIZE)] for _ in range(MAZE_SIZE)]
        self.pit = 3 * [];
        self.weapon = 3 * [];
        self.player = PLAYER_COUNT * [None];
        self.generate()

    def getWallChance(self):
        return random.random() > WALL_CHANCE

    def get_random_pos(self):
        x = random.randrange(MAZE_SIZE-1)
        y = random.randrange(MAZE_SIZE-1)
        return (y, x)

    def generate(self):
        for y in range(MAZE_SIZE):
            for x in range(MAZE_SIZE):
                self.map[y][x] = Cell()

        for x in range(MAZE_SIZE):
            self.map[0][x].setWall('up')
            self.map[MAZE_SIZE-1][x].setWall('down')
            self.map[x][MAZE_SIZE-1].setWall('right')
            self.map[x][0].setWall('left')

        group = MAZE_SIZE * [None]

        i = MAZE_SIZE
        for x in range(MAZE_SIZE):
            group[x] = x

        for y in range(MAZE_SIZE - 1):
            for x in range(MAZE_SIZE - 1):
                if group[x+1] == group[x]:
                    if self.getWallChance():
                        self.map[y][x].setWall('right')
                        self.map[y][x+1].setWall('left')
                        if (self.map[y][x].hasWall('up')) or (self.map[y][x+1].hasWall('up')):
                            group[x+1] = i
                            i = i + 1
                else:
                    if self.getWallChance():
                        self.map[y][x].setWall('right')
                        self.map[y][x+1].setWall('left')
                        group[x+1] = i
                        i = i + 1
                    else:
                        group[x+1] = group[x]

            f = False
            for x in range(MAZE_SIZE-1):
                if group[x] == group[x+1]:
                    if self.getWallChance():
                        self.map[y][x].setWall('down')
                        self.map[y+1][x].setWall('up')
                    else:
                        f = True
                else:
                    if f:
                        if self.getWallChance():
                            self.map[y][x].setWall('down')
                            self.map[y+1][x].setWall('up')
                    f = False
            if group[MAZE_SIZE-2] == group[MAZE_SIZE-1] and f:
                if self.getWallChance():
                    self.map[y][MAZE_SIZE-1].setWall('down')
                    self.map[y+1][MAZE_SIZE-1].setWall('up')

            #for x in range(MAZE_SIZE):
                #if self.map[y][x].hasWall('down'):
                    #group[x] = i
                    #i = i + 1
            #print(group)
        for x in range(MAZE_SIZE-1):
            if group[x] == group[x+1] and not self.map[MAZE_SIZE-1][x].hasWall('up') and not self.map[MAZE_SIZE-1][x+1].hasWall('up'):
                if self.getWallChance():
                    self.map[MAZE_SIZE-1][x].setWall('right')
                    self.map[MAZE_SIZE-1][x+1].setWall('left')
        # set exit
        if (random.choice([True, False])):
            x = random.choice([0, MAZE_SIZE-1])
            y = random.randrange(MAZE_SIZE - 2) + 1
        else:
            x = random.randrange(MAZE_SIZE - 2) + 1
            y = random.choice([0, MAZE_SIZE-1])
        self.exit = (y, x)
        self.map[y][x].setItem(1)

        # players
        for i in range(PLAYER_COUNT):
            res = False
            while not res:
                new_pos = self.get_random_pos()
                if self.map[new_pos[0]][new_pos[1]].setItem(1):
                    res = True
            self.player[i] = new_pos

