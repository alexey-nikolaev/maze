from cell import Cell
from maze import Maze
import pygame
from config import *

def printMaze(maze):
    map_size = SIZE * 2 - 1
    maze_map = [[0 for _ in range(map_size)] for _ in range(map_size)]

    for y in range(SIZE-1):
        for x in range(SIZE-1):
            if maze[y][x].getWall('right'):
                maze_map[y * 2][x * 2 + 1] = 1
            else:
                maze_map[y * 2][x * 2 + 1] = 3
            if maze[y][x].getWall('down'):
                maze_map[y * 2 + 1][x * 2] = 2
            else:
                maze_map[y * 2 + 1][x * 2] = 3
        if maze[y][SIZE-1].getWall('down'):
            maze_map[y * 2 + 1][(SIZE-1) * 2] = 2
        else:
            maze_map[y * 2 + 1][(SIZE-1) * 2] = 3
        for x in range(SIZE-1):
            if maze[SIZE-1][x].getWall('right'):
                maze_map[(SIZE-1) * 2][x * 2 + 1] = 1
            else:
                maze_map[(SIZE-1) * 2][x * 2 + 1] = 3
    for y in range(SIZE * 2 - 1):
        str_line = ''
        for x in range(SIZE * 2 - 1):
            if maze_map[y][x] == 1:
                str_line = str_line + '|'
            elif maze_map[y][x] == 2:
                str_line = str_line + '-'
            elif maze_map[y][x] == 3:
                str_line = str_line + '.'
            else:
                str_line = str_line + '.'
        print(str_line)

def paintMaze(screen, maze):
    screen.fill((0, 0, 0))
    for y in range(MAZE_SIZE):
        for x in range(MAZE_SIZE):
            if maze.map[y][x].getWall('left'):
                pygame.draw.line(screen, (255, 255, 255), (x*WALL_SIZE, y*WALL_SIZE), (x*WALL_SIZE, (y+1)*WALL_SIZE), 2)
            if maze.map[y][x].getWall('right'):
                pygame.draw.line(screen, (255, 255, 255), ((x+1)*WALL_SIZE, y*WALL_SIZE), ((x+1)*WALL_SIZE, (y+1)*WALL_SIZE), 2)
            if maze.map[y][x].getWall('down'):
                pygame.draw.line(screen, (255, 255, 255), (x*WALL_SIZE, (y+1)*WALL_SIZE), ((x+1)*WALL_SIZE, (y+1)*WALL_SIZE), 2)
            if maze.map[y][x].getWall('up'):
                pygame.draw.line(screen, (255, 255, 255), (x*WALL_SIZE, y*WALL_SIZE), ((x+1)*WALL_SIZE, y*WALL_SIZE), 2)

    font = pygame.font.SysFont("monospace", 15)
    exit_cell = (maze.exit[1]*WALL_SIZE + 6, maze.exit[0]*WALL_SIZE + 2)
    label = font.render("E", 1, (255,255,0))
    screen.blit(label, (exit_cell))

    print('********')
    for i in maze.player:
        print(i)
        p_cell = (i[1]*WALL_SIZE + 7, i[0]*WALL_SIZE + 5)
        label = font.render("*", 1, (255,0,255))
        screen.blit(label, p_cell)

    pygame.display.flip()

def rotate(maze, r_dir):
    r_maze = [[0 for _ in range(SIZE)] for _ in range(SIZE)]
    if r_dir == 1:
        for y in range(SIZE):
            for x in range(SIZE):
                r_maze[y][x] = maze[SIZE - x - 1][y]
    elif r_dir == 2:
        for y in range(SIZE):
            for x in range(SIZE):
                r_maze[y][x] = maze[SIZE - y - 1][SIZE - x - 1]
    elif r_dir == 3:
        for y in range(SIZE):
            for x in range(SIZE):
                r_maze[y][x] = maze[x][SIZE - y - 1]
    else:
        return maze
    return r_maze

#maze = rotate(maze, random.randrange(4))
maze = Maze()

#printMaze(maze.map)

pygame.init()

size = [WALL_SIZE * MAZE_SIZE, WALL_SIZE * MAZE_SIZE]
screen = pygame.display.set_mode(size)

paintMaze(screen, maze)

main_loop = True
while main_loop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            main_loop = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            maze.generate()
            paintMaze(screen, maze)

pygame.quit()
