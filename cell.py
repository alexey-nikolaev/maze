class Cell():

    def __init__(self):
        self.walls = {'up': False, 'right': False, 'down': False, 'left': False}
        self.item = None

    def getWall(self, direction):
        return self.walls[direction]

    def hasWall(self, direction):
        return self.walls[direction]

    def setWall(self, direction):
        self.walls[direction] = True

    def resetWall(self, direction):
        self.walls[direction] = False

    def hasItem(self):
        if self.item:
            return True
        return False

    def setItem(self, item):
        if self.hasItem():
            return False
        self.item = item
        return True
